/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 17 "cfgram.y" /* yacc.c:339  */

#include <rush.h>
#include <cf.h>
static int errors;
int re_flags = REG_EXTENDED;
static struct rush_rule *current_rule;
struct asgn {
	struct asgn *next;
	char *name;
	char *value;
};
static void add_asgn_list(struct asgn *head, enum envar_type type);
static struct transform_node *new_set_node(enum transform_node_type type,
					   char *varname,
					   struct cfloc const *loc);

#line 83 "cfgram.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_CFGRAM_H_INCLUDED
# define YY_YY_CFGRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    STRING = 258,
    IDENT = 259,
    NUMBER = 260,
    RUSH = 261,
    T_VERSION = 262,
    RULE = 263,
    GLOBAL = 264,
    EOL = 265,
    SET = 266,
    INSERT = 267,
    REMOPT = 268,
    MAP = 269,
    UNSET = 270,
    MATCH = 271,
    FALLTHROUGH = 272,
    INCLUDE = 273,
    LIMITS = 274,
    CLRENV = 275,
    SETENV = 276,
    UNSETENV = 277,
    KEEPENV = 278,
    EVALENV = 279,
    DELETE = 280,
    EXIT = 281,
    ATTRIB = 282,
    GLATTRIB = 283,
    BOGUS = 284,
    OR = 285,
    AND = 286,
    NOT = 287,
    EQ = 288,
    NE = 289,
    LT = 290,
    LE = 291,
    GT = 292,
    GE = 293,
    XF = 294,
    NM = 295,
    IN = 296,
    GROUP = 297,
    TEST = 298
  };
#endif
/* Tokens.  */
#define STRING 258
#define IDENT 259
#define NUMBER 260
#define RUSH 261
#define T_VERSION 262
#define RULE 263
#define GLOBAL 264
#define EOL 265
#define SET 266
#define INSERT 267
#define REMOPT 268
#define MAP 269
#define UNSET 270
#define MATCH 271
#define FALLTHROUGH 272
#define INCLUDE 273
#define LIMITS 274
#define CLRENV 275
#define SETENV 276
#define UNSETENV 277
#define KEEPENV 278
#define EVALENV 279
#define DELETE 280
#define EXIT 281
#define ATTRIB 282
#define GLATTRIB 283
#define BOGUS 284
#define OR 285
#define AND 286
#define NOT 287
#define EQ 288
#define NE 289
#define LT 290
#define LE 291
#define GT 292
#define GE 293
#define XF 294
#define NM 295
#define IN 296
#define GROUP 297
#define TEST 298

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 37 "cfgram.y" /* yacc.c:355  */

	char *str;
	struct cfnumber num;
	int intval;
	regex_t regex;
	struct rush_rule rule;
	struct test_node *node;
	struct strlist {
		char **argv;
		size_t argc;
	} strlist;
	struct {
		int start;
		int end;
	} range;
	struct asgn *asgn;
	struct {
		struct asgn *head;
		struct asgn *tail;
	} asgn_list;
	struct limits_rec *lrec;
	rule_attrib_setter_t attrib;
	struct global_attrib *global_attrib;
	struct argval *arg;
	struct {
		int argc;
		struct argval *head;
		struct argval *tail;
	} arglist;
	struct { unsigned major, minor; } version;
	int fstest;

#line 242 "cfgram.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_CFGRAM_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 273 "cfgram.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   206

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  45
/* YYNRULES -- Number of rules.  */
#define YYNRULES  109
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  196

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   298

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      45,    46,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    47,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    48,     2,    49,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,    44,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   132,   132,   135,   140,   147,   158,   159,   162,   163,
     166,   167,   170,   171,   174,   175,   178,   181,   182,   185,
     195,   200,   208,   216,   227,   237,   240,   243,   244,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     270,   282,   283,   289,   297,   298,   303,   309,   316,   326,
     333,   340,   347,   354,   361,   368,   375,   382,   389,   396,
     401,   409,   410,   413,   414,   415,   421,   436,   447,   458,
     470,   482,   494,   503,   514,   525,   533,   550,   568,   587,
     590,   593,   600,   606,   610,   615,   628,   631,   641,   658,
     663,   678,   689,   695,   715,   740,   778,   781,   787,   791,
     800,   808,   812,   818,   822,   829,   836,   848,   855,   855
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"string\"", "\"identifier\"",
  "\"number\"", "\"rush\"", "T_VERSION", "\"rule\"", "\"global\"",
  "\"end of line\"", "\"set\"", "\"insert\"", "\"remopt\"", "\"map\"",
  "\"unset\"", "\"match\"", "\"fallthrough\"", "\"include\"", "\"limits\"",
  "\"clrenv\"", "\"setenv\"", "\"unsetenv\"", "\"keepenv\"", "\"evalenv\"",
  "\"delete\"", "\"exit\"", "\"rule attribute\"", "\"global attribute\"",
  "\"erroneous token\"", "\"||\"", "\"&&\"", "\"!\"", "\"==\"", "\"!=\"",
  "\"<\"", "\"<=\"", "\">\"", "\">=\"", "\"=~\"", "\"!~\"", "\"in\"",
  "\"group\"", "\"test\"", "'~'", "'('", "')'", "'='", "'['", "']'",
  "$accept", "rcfile", "select", "preface", "skipeol", "eol", "content",
  "rulelist", "rule", "globhdr", "globbody", "glob_stmt", "arglist", "arg",
  "rulehdr", "ruleid", "rulebody", "stmt", "match_stmt", "compound_cond",
  "simple_cond", "expr", "literal", "string", "regex", "set_stmt",
  "map_stmt", "defval", "index", "value", "flowctl_stmt", "fdescr",
  "delete_stmt", "range", "include_stmt", "limits_stmt", "resource_limits",
  "remopt_stmt", "optstring", "environ_stmt", "asgn_list", "asgn",
  "attrib_stmt", "strlist", "$@1", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   126,    40,    41,    61,    91,    93
};
# endif

#define YYPACT_NINF -85

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-85)))

#define YYTABLE_NINF -15

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
       9,   -85,    26,     3,    14,   -85,    36,   -85,   -85,     9,
     -85,    38,    27,   -85,    86,     9,   -85,    27,   -85,    30,
     168,   -85,   -85,   -85,     9,   -85,    14,   -85,    95,    30,
     -85,   -85,     1,    20,    86,     7,     8,    92,   -85,    86,
      69,   -85,    75,    59,    59,    86,    87,    97,    86,   131,
     -85,     9,     9,     9,     9,     9,     9,     9,     9,     9,
       9,    14,   -85,   -85,   -85,   106,   -85,   -85,   -85,   -26,
      98,   -17,    68,    86,    86,    86,   -85,   -85,    92,    24,
      86,    92,    40,   -85,   -85,   126,   116,   -85,   115,    82,
      89,   -85,    59,   -85,    59,   -85,   123,   -85,   -85,    81,
     -85,   -85,    14,    14,    14,    14,    14,   -85,    14,    14,
      14,    14,    14,   -85,    86,    86,    84,    86,    86,    86,
     -85,   -85,   -85,    86,    86,   -85,   -85,   -85,   -85,   -85,
     -13,    92,    92,   109,   118,   133,   136,   160,   163,    86,
     127,    86,   -85,   -85,    86,    86,   -85,   -85,   -85,   -85,
     -85,   129,   -85,   -85,   -85,   130,   -85,   132,   -85,    86,
      86,    95,   -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,
     -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,    86,
      86,    86,   166,   170,    11,   -85,   -85,   -85,   191,   192,
     -85,    86,    86,   -85,   -85,   -85
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       6,     8,     0,     0,     7,     1,     0,     4,     2,     6,
       9,     0,    10,     5,    25,     0,     3,    11,    12,     0,
       0,    64,    63,    65,     0,    26,    16,    13,     0,    15,
      17,    39,     0,     0,     0,     0,     0,     0,    83,     0,
       0,    98,     0,     0,     0,     0,     0,    86,     0,     0,
      27,     0,     0,     0,     0,     0,     6,     0,     0,     0,
       0,    24,    62,    61,    23,     0,    20,    22,    18,     0,
       0,     0,     0,    96,     0,     0,    75,    76,     0,     0,
       0,     0,    40,    41,    44,     0,     0,    93,    92,     0,
      61,   105,   101,   103,   102,   100,    89,    88,    87,     0,
     107,    28,    29,    30,    31,    35,    32,    38,    33,    37,
      34,    36,    19,    21,     0,     0,     0,     0,     0,     0,
      97,    95,    82,     0,     0,    45,   108,    58,    59,    60,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    91,    94,     0,     0,   104,    90,    84,    85,
      73,    82,    72,    81,    69,    82,    67,    82,    68,     0,
       0,     0,    46,    43,    42,    51,    49,    52,    50,    53,
      54,    55,    56,    66,    48,    57,    47,    99,   106,     0,
       0,     0,     0,     0,     0,    74,    70,    71,     0,     0,
     109,    79,    79,    80,    78,    77
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -85,   -85,   -85,   -85,    -3,    23,   -85,   -85,   181,   -85,
     -85,   171,    41,   -64,   -85,   -85,   -85,   150,   -85,   120,
     -71,   -85,   -40,   -14,    62,   -85,   -85,    12,     4,   -73,
     -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,   -85,
     161,   -84,   -85,    66,   -85
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     8,     9,     3,     4,    16,    17,    18,    19,
      29,    30,    65,    66,    20,    24,    49,    50,    51,    82,
      83,    84,    67,   122,   174,    52,    53,   194,    71,   123,
      54,    99,    55,    97,    56,    57,    88,    58,   121,    59,
      92,    93,    60,   128,   161
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      25,   113,   124,    91,    91,    69,    12,   125,   146,     6,
     146,    74,    76,   114,    62,    63,    64,   131,   132,     1,
      73,   115,   117,    85,    10,    86,     5,    21,    22,    23,
     118,    95,     7,   162,   100,    14,    15,    72,    26,    75,
      77,   150,   152,    11,   154,   156,   158,    61,    13,    70,
     159,   160,    91,   107,    91,    70,    70,   190,    28,   120,
     163,   164,    62,    90,    85,   127,   129,    85,    70,   126,
     131,   132,   178,    87,   102,   103,   104,   105,   106,    89,
     108,   109,   110,   111,   148,   149,   182,   183,   112,    21,
      22,    23,    96,   166,   168,    21,    22,    23,    62,    63,
      64,   151,    98,   116,   155,   157,   185,   186,   187,    62,
      63,    64,    62,    63,   165,   119,     1,    85,    85,   143,
     113,    62,    63,   167,    78,   173,   142,   173,   147,   144,
     177,   -14,    31,   153,    79,    80,   145,    81,   169,   -14,
     -14,   170,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,   133,
     134,   135,   136,   137,   138,   171,   139,   140,   172,    31,
     141,   188,   126,   179,   180,   189,   181,   193,   193,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,   191,   192,    27,   101,
      68,   130,   184,   176,   195,    94,   175
};

static const yytype_uint8 yycheck[] =
{
      14,    65,    75,    43,    44,     4,     9,    78,    92,     6,
      94,     4,     4,    39,     3,     4,     5,    30,    31,    10,
      34,    47,    39,    37,    10,    39,     0,     3,     4,     5,
      47,    45,    29,    46,    48,     8,     9,    33,    15,    35,
      36,   114,   115,     7,   117,   118,   119,    24,    10,    48,
     123,   124,    92,    56,    94,    48,    48,    46,    28,    73,
     131,   132,     3,     4,    78,    79,    80,    81,    48,    45,
      30,    31,   145,     4,    51,    52,    53,    54,    55,     4,
      57,    58,    59,    60,     3,     4,   159,   160,    65,     3,
       4,     5,     5,   133,   134,     3,     4,     5,     3,     4,
       5,   115,     5,     5,   118,   119,   179,   180,   181,     3,
       4,     5,     3,     4,     5,    47,    10,   131,   132,     4,
     184,     3,     4,     5,    32,   139,    10,   141,     5,    47,
     144,     0,     1,    49,    42,    43,    47,    45,     5,     8,
       9,     5,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    33,
      34,    35,    36,    37,    38,     5,    40,    41,     5,     1,
      44,     5,    45,    44,    44,     5,    44,   191,   192,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,     5,     5,    17,    49,
      29,    81,   161,   141,   192,    44,   140
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    10,    51,    54,    55,     0,     6,    29,    52,    53,
      10,     7,    54,    10,     8,     9,    56,    57,    58,    59,
      64,     3,     4,     5,    65,    73,    55,    58,    28,    60,
      61,     1,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    66,
      67,    68,    75,    76,    80,    82,    84,    85,    87,    89,
      92,    55,     3,     4,     5,    62,    63,    72,    61,     4,
      48,    78,    78,    73,     4,    78,     4,    78,    32,    42,
      43,    45,    69,    70,    71,    73,    73,     4,    86,     4,
       4,    72,    90,    91,    90,    73,     5,    83,     5,    81,
      73,    67,    55,    55,    55,    55,    55,    54,    55,    55,
      55,    55,    55,    63,    39,    47,     5,    39,    47,    47,
      73,    88,    73,    79,    79,    70,    45,    73,    93,    73,
      69,    30,    31,    33,    34,    35,    36,    37,    38,    40,
      41,    44,    10,     4,    47,    47,    91,     5,     3,     4,
      79,    73,    79,    49,    79,    73,    79,    73,    79,    79,
      79,    94,    46,    70,    70,     5,    72,     5,    72,     5,
       5,     5,     5,    73,    74,    93,    74,    73,    79,    44,
      44,    44,    79,    79,    62,    79,    79,    79,     5,     5,
      46,     5,     5,    73,    77,    77
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    51,    52,    52,    53,    54,    54,    55,    55,
      56,    56,    57,    57,    58,    58,    59,    60,    60,    61,
      62,    62,    63,    63,    64,    65,    65,    66,    66,    67,
      67,    67,    67,    67,    67,    67,    67,    67,    67,    67,
      68,    69,    69,    69,    70,    70,    70,    71,    71,    71,
      71,    71,    71,    71,    71,    71,    71,    71,    71,    71,
      71,    72,    72,    73,    73,    73,    74,    75,    75,    75,
      75,    75,    75,    75,    75,    75,    75,    76,    76,    77,
      77,    78,    79,    80,    80,    80,    81,    81,    82,    83,
      83,    84,    85,    86,    86,    87,    88,    88,    89,    89,
      89,    89,    89,    90,    90,    91,    91,    92,    94,    93
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     3,     1,     3,     0,     1,     1,     2,
       0,     1,     1,     2,     2,     2,     2,     1,     2,     3,
       1,     2,     1,     1,     3,     0,     1,     1,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     1,
       2,     1,     3,     3,     1,     2,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     2,     2,
       2,     1,     1,     1,     1,     1,     1,     4,     4,     4,
       6,     6,     4,     4,     6,     2,     2,     8,     8,     0,
       1,     3,     1,     1,     3,     3,     0,     1,     2,     1,
       2,     3,     2,     1,     2,     3,     0,     1,     1,     4,
       2,     2,     2,     1,     2,     1,     3,     2,     0,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 136 "cfgram.y" /* yacc.c:1646  */
    {
		     if (errors)
			     YYERROR;
	     }
#line 1594 "cfgram.c" /* yacc.c:1646  */
    break;

  case 4:
#line 141 "cfgram.y" /* yacc.c:1646  */
    {
		     if (parse_old_rc())
			     YYERROR;
	     }
#line 1603 "cfgram.c" /* yacc.c:1646  */
    break;

  case 5:
#line 148 "cfgram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[-1].version).major == 2 && (yyvsp[-1].version).minor == 0) {
			     cflex_normal();
		     } else {
			     cferror(&(yylsp[-1]), _("unsupported configuration file version"));
			     YYERROR;
		     }
	     }
#line 1616 "cfgram.c" /* yacc.c:1646  */
    break;

  case 19:
#line 186 "cfgram.y" /* yacc.c:1646  */
    {
		     struct cfloc loc;
		     loc.beg = (yylsp[-2]).beg;
		     loc.end = (yylsp[-1]).end;
		     global_attrib_set((yyvsp[-2].global_attrib), (yyvsp[-1].arglist).argc, (yyvsp[-1].arglist).head, &loc);
		     arglist_free((yyvsp[-1].arglist).head);
	     }
#line 1628 "cfgram.c" /* yacc.c:1646  */
    break;

  case 20:
#line 196 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.arglist).head = (yyval.arglist).tail = (yyvsp[0].arg);
		     (yyval.arglist).argc = 1;
	     }
#line 1637 "cfgram.c" /* yacc.c:1646  */
    break;

  case 21:
#line 201 "cfgram.y" /* yacc.c:1646  */
    {
		     LIST_APPEND((yyvsp[0].arg), (yyvsp[-1].arglist).head, (yyvsp[-1].arglist).tail);
		     (yyvsp[-1].arglist).argc++;
		     (yyval.arglist) = (yyvsp[-1].arglist);
	     }
#line 1647 "cfgram.c" /* yacc.c:1646  */
    break;

  case 22:
#line 209 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.arg) = xcalloc(1, sizeof(*(yyval.arg)));
		     (yyval.arg)->next = NULL;
		     (yyval.arg)->loc = (yylsp[0]);
		     (yyval.arg)->isnum = 0;
		     (yyval.arg)->strval = (yyvsp[0].str);
	     }
#line 1659 "cfgram.c" /* yacc.c:1646  */
    break;

  case 23:
#line 217 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.arg) = xcalloc(1, sizeof(*(yyval.arg)));
		     (yyval.arg)->next = NULL;
		     (yyval.arg)->loc = (yylsp[0]);
		     (yyval.arg)->isnum = 1;
		     (yyval.arg)->strval = (yyvsp[0].num).strval;
		     (yyval.arg)->intval = (yyvsp[0].num).intval;
	     }
#line 1672 "cfgram.c" /* yacc.c:1646  */
    break;

  case 24:
#line 228 "cfgram.y" /* yacc.c:1646  */
    {
		     current_rule = new_rush_rule((yyvsp[-1].str));
		     current_rule->file = (yylsp[-2]).beg.filename;
		     current_rule->line = (yylsp[-2]).beg.line;
		     free((yyvsp[-1].str));
	     }
#line 1683 "cfgram.c" /* yacc.c:1646  */
    break;

  case 25:
#line 237 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.str) = NULL;
	     }
#line 1691 "cfgram.c" /* yacc.c:1646  */
    break;

  case 39:
#line 258 "cfgram.y" /* yacc.c:1646  */
    {
		     skiptoeol();
		     restorenormal();
		     yyerrok;
		     yyclearin;
		     errors = 1;
	     }
#line 1703 "cfgram.c" /* yacc.c:1646  */
    break;

  case 40:
#line 271 "cfgram.y" /* yacc.c:1646  */
    {
		     if (current_rule->test_node) {
			     struct test_node *np = new_test_node(test_and);
			     np->v.arg[0] = current_rule->test_node;
			     np->v.arg[1] = (yyvsp[0].node);
			     current_rule->test_node = np;
		     } else
			     current_rule->test_node = (yyvsp[0].node);
	     }
#line 1717 "cfgram.c" /* yacc.c:1646  */
    break;

  case 42:
#line 284 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_and);
		     (yyval.node)->v.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.arg[1] = (yyvsp[0].node);
	     }
#line 1727 "cfgram.c" /* yacc.c:1646  */
    break;

  case 43:
#line 290 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_or);
		     (yyval.node)->v.arg[0] = (yyvsp[-2].node);
		     (yyval.node)->v.arg[1] = (yyvsp[0].node);
	     }
#line 1737 "cfgram.c" /* yacc.c:1646  */
    break;

  case 45:
#line 299 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_not);
		     (yyval.node)->v.arg[0] = (yyvsp[0].node);
	     }
#line 1746 "cfgram.c" /* yacc.c:1646  */
    break;

  case 46:
#line 304 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = (yyvsp[-1].node);
	     }
#line 1754 "cfgram.c" /* yacc.c:1646  */
    break;

  case 47:
#line 310 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmps);
		     (yyval.node)->v.cmp.op = cmp_match;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.rx = (yyvsp[0].regex);
	     }
#line 1765 "cfgram.c" /* yacc.c:1646  */
    break;

  case 48:
#line 317 "cfgram.y" /* yacc.c:1646  */
    {
		     struct test_node *np = new_test_node(test_cmps);
		     np->v.cmp.op = cmp_match;
		     np->v.cmp.larg = (yyvsp[-2].str);
		     np->v.cmp.rarg.rx = (yyvsp[0].regex);

		     (yyval.node) = new_test_node(test_not);
		     (yyval.node)->v.arg[0] = np;
	     }
#line 1779 "cfgram.c" /* yacc.c:1646  */
    break;

  case 49:
#line 327 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmps);
		     (yyval.node)->v.cmp.op = cmp_eq;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.str = (yyvsp[0].str);
	     }
#line 1790 "cfgram.c" /* yacc.c:1646  */
    break;

  case 50:
#line 334 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmps);
		     (yyval.node)->v.cmp.op = cmp_ne;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.str = (yyvsp[0].str);
	     }
#line 1801 "cfgram.c" /* yacc.c:1646  */
    break;

  case 51:
#line 341 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_eq;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1812 "cfgram.c" /* yacc.c:1646  */
    break;

  case 52:
#line 348 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_ne;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1823 "cfgram.c" /* yacc.c:1646  */
    break;

  case 53:
#line 355 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_lt;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1834 "cfgram.c" /* yacc.c:1646  */
    break;

  case 54:
#line 362 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_le;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1845 "cfgram.c" /* yacc.c:1646  */
    break;

  case 55:
#line 369 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_gt;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1856 "cfgram.c" /* yacc.c:1646  */
    break;

  case 56:
#line 376 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_cmpn);
		     (yyval.node)->v.cmp.op = cmp_ge;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.num = (yyvsp[0].num).intval;
	     }
#line 1867 "cfgram.c" /* yacc.c:1646  */
    break;

  case 57:
#line 383 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_in);
		     (yyval.node)->v.cmp.op = cmp_in;
		     (yyval.node)->v.cmp.larg = (yyvsp[-2].str);
		     (yyval.node)->v.cmp.rarg.strv = (yyvsp[0].strlist).argv;
	     }
#line 1878 "cfgram.c" /* yacc.c:1646  */
    break;

  case 58:
#line 390 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_group);
		     (yyval.node)->v.groups = xcalloc(2, sizeof((yyval.node)->v.groups[0]));
		     (yyval.node)->v.groups[0] = (yyvsp[0].str);
		     (yyval.node)->v.groups[1] = NULL;
	     }
#line 1889 "cfgram.c" /* yacc.c:1646  */
    break;

  case 59:
#line 397 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_group);
		     (yyval.node)->v.groups = (yyvsp[0].strlist).argv;
	     }
#line 1898 "cfgram.c" /* yacc.c:1646  */
    break;

  case 60:
#line 402 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.node) = new_test_node(test_fstest);
		     (yyval.node)->v.fstest.op = (yyvsp[-1].fstest);
		     (yyval.node)->v.fstest.arg = (yyvsp[0].str);
	     }
#line 1908 "cfgram.c" /* yacc.c:1646  */
    break;

  case 65:
#line 416 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.str) = (yyvsp[0].num).strval;
	     }
#line 1916 "cfgram.c" /* yacc.c:1646  */
    break;

  case 66:
#line 422 "cfgram.y" /* yacc.c:1646  */
    {
		     int rc = regcomp(&(yyval.regex), (yyvsp[0].str), re_flags);
		     if (rc) {
			     char errbuf[512];
			     regerror(rc, &(yyval.regex), errbuf, sizeof(errbuf));
			     cferror(&(yylsp[0]), _("invalid regexp: %s"), (yyvsp[0].str));
			     YYERROR;
		     }
	     }
#line 1930 "cfgram.c" /* yacc.c:1646  */
    break;

  case 67:
#line 437 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_set);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 0;
		     node->target.v.arg.idx = (yyvsp[-2].intval);
		     node->v.xf.pattern = (yyvsp[0].str);
		     node->v.xf.trans = NULL;
	     }
#line 1945 "cfgram.c" /* yacc.c:1646  */
    break;

  case 68:
#line 448 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_set);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 1;
		     node->target.v.arg.idx = (yyvsp[-2].intval);
		     node->v.xf.pattern = (yyvsp[0].str);
		     node->v.xf.trans = NULL;
	     }
#line 1960 "cfgram.c" /* yacc.c:1646  */
    break;

  case 69:
#line 459 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_set);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 0;
		     node->target.v.arg.idx = (yyvsp[-2].intval);
		     node->v.xf.pattern = NULL;
		     node->v.xf.trans = compile_transform_expr((yyvsp[0].str), re_flags,
							       &(yylsp[0]));
	     }
#line 1976 "cfgram.c" /* yacc.c:1646  */
    break;

  case 70:
#line 471 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_set);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 0;
		     node->target.v.arg.idx = (yyvsp[-4].intval);
		     node->v.xf.pattern = (yyvsp[-2].str);
		     node->v.xf.trans = compile_transform_expr((yyvsp[0].str), re_flags,
							       &(yylsp[0]));
	     }
#line 1992 "cfgram.c" /* yacc.c:1646  */
    break;

  case 71:
#line 483 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_set);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 1;
		     node->target.v.arg.idx = (yyvsp[-4].intval);
		     node->v.xf.pattern = (yyvsp[-2].str);
		     node->v.xf.trans = compile_transform_expr((yyvsp[0].str), re_flags,
							       &(yylsp[0]));
	     }
#line 2008 "cfgram.c" /* yacc.c:1646  */
    break;

  case 72:
#line 495 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node =
			     new_set_node(transform_set, (yyvsp[-2].str), &(yylsp[-2]));
		     if (node) {
			     node->v.xf.pattern = (yyvsp[0].str);
			     node->v.xf.trans = NULL;
		     }
	     }
#line 2021 "cfgram.c" /* yacc.c:1646  */
    break;

  case 73:
#line 504 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node =
			     new_set_node(transform_set, (yyvsp[-2].str), &(yylsp[-2]));
		     if (node) {
			     node->v.xf.pattern = NULL;
			     node->v.xf.trans = compile_transform_expr((yyvsp[0].str),
								       re_flags,
								       &(yylsp[0]));
		     }
	     }
#line 2036 "cfgram.c" /* yacc.c:1646  */
    break;

  case 74:
#line 515 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node =
			     new_set_node(transform_set, (yyvsp[-4].str), &(yylsp[-4]));
		     if (node) {
			     node->v.xf.pattern = (yyvsp[-2].str);
			     node->v.xf.trans = compile_transform_expr((yyvsp[0].str),
								       re_flags,
								       &(yylsp[0]));
		     }
	     }
#line 2051 "cfgram.c" /* yacc.c:1646  */
    break;

  case 75:
#line 526 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node =
			     new_set_node(transform_delete, (yyvsp[0].str), &(yylsp[0]));
		     if (node) {
			     node->target.v.name = (yyvsp[0].str);
		     }
	     }
#line 2063 "cfgram.c" /* yacc.c:1646  */
    break;

  case 76:
#line 534 "cfgram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].intval) == 0) {
			     cferror(&(yylsp[0]), _("$0 cannot be unset"));
			     errors++;
		     } else {
			     struct transform_node *node =
				     new_transform_node(current_rule,
							transform_delete);
			     node->target.type = target_arg;
			     node->target.v.arg.ins = 0;
			     node->target.v.arg.idx = (yyvsp[0].intval);
			     node->v.arg_end = (yyvsp[0].intval);
		     }
	     }
#line 2082 "cfgram.c" /* yacc.c:1646  */
    break;

  case 77:
#line 551 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_transform_node(current_rule, transform_map);
		     node->target.type = target_arg;
		     node->target.v.arg.ins = 0;
		     node->target.v.arg.idx = (yyvsp[-6].intval);
		     node->v.map.file = (yyvsp[-5].str);
		     node->v.map.delim = (yyvsp[-4].str);
		     node->v.map.key = (yyvsp[-3].str);
		     node->v.map.key_field = (yyvsp[-2].num).intval;
		     node->v.map.val_field = (yyvsp[-1].num).intval;
		     node->v.map.defval = (yyvsp[0].str);

		     free((yyvsp[-2].num).strval);
		     free((yyvsp[-1].num).strval);
	     }
#line 2104 "cfgram.c" /* yacc.c:1646  */
    break;

  case 78:
#line 569 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;

		     node = new_set_node(transform_map, (yyvsp[-6].str), &(yylsp[-6]));
		     node->target.v.name = (yyvsp[-6].str);
		     node->v.map.file = (yyvsp[-5].str);
		     node->v.map.delim = (yyvsp[-4].str);
		     node->v.map.key = (yyvsp[-3].str);
		     node->v.map.key_field = (yyvsp[-2].num).intval;
		     node->v.map.val_field = (yyvsp[-1].num).intval;
		     node->v.map.defval = (yyvsp[0].str);

		     free((yyvsp[-2].num).strval);
		     free((yyvsp[-1].num).strval);
	     }
#line 2124 "cfgram.c" /* yacc.c:1646  */
    break;

  case 79:
#line 587 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.str) = NULL;
	     }
#line 2132 "cfgram.c" /* yacc.c:1646  */
    break;

  case 81:
#line 594 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.intval) = (yyvsp[-1].num).intval;
		     free((yyvsp[-1].num).strval);
	     }
#line 2141 "cfgram.c" /* yacc.c:1646  */
    break;

  case 83:
#line 607 "cfgram.y" /* yacc.c:1646  */
    {
		     current_rule->fall_through = 1;
	     }
#line 2149 "cfgram.c" /* yacc.c:1646  */
    break;

  case 84:
#line 611 "cfgram.y" /* yacc.c:1646  */
    {
		     current_rule->error = new_error((yyvsp[-1].intval), (yyvsp[0].str), 0);
		     free((yyvsp[0].str));
	     }
#line 2158 "cfgram.c" /* yacc.c:1646  */
    break;

  case 85:
#line 616 "cfgram.y" /* yacc.c:1646  */
    {
		     int n = string_to_error_index((yyvsp[0].str));
		     if (n == -1) {
			     cferror(&(yylsp[-2]), _("Unknown message reference"));
			     YYERROR;
		     } else
			     current_rule->error = new_standard_error((yyvsp[-1].intval), n);
		     free((yyvsp[0].str));
	     }
#line 2172 "cfgram.c" /* yacc.c:1646  */
    break;

  case 86:
#line 628 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.intval) = 2;
	     }
#line 2180 "cfgram.c" /* yacc.c:1646  */
    break;

  case 87:
#line 632 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.intval) = (yyvsp[0].num).intval;
		     free((yyvsp[0].num).strval);
	     }
#line 2189 "cfgram.c" /* yacc.c:1646  */
    break;

  case 88:
#line 642 "cfgram.y" /* yacc.c:1646  */
    {
		     if ((yyvsp[0].range).start == 0 || (yyvsp[0].range).end == 0) {
			     cferror(&(yylsp[0]), _("$0 cannot be deleted"));
			     errors++;
		     } else {
			     struct transform_node *node =
				     new_transform_node(current_rule,
							transform_delete);
			     node->target.type = target_arg;
			     node->target.v.arg.ins = 0;
			     node->target.v.arg.idx = (yyvsp[0].range).start;
			     node->v.arg_end = (yyvsp[0].range).end;
		     }
	     }
#line 2208 "cfgram.c" /* yacc.c:1646  */
    break;

  case 89:
#line 659 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.range).start = (yyval.range).end = (yyvsp[0].num).intval;
		     free((yyvsp[0].num).strval);
	     }
#line 2217 "cfgram.c" /* yacc.c:1646  */
    break;

  case 90:
#line 664 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.range).start = (yyvsp[-1].num).intval;
		     (yyval.range).end = (yyvsp[0].num).intval;
		     free((yyvsp[-1].num).strval);
		     free((yyvsp[0].num).strval);
	     }
#line 2228 "cfgram.c" /* yacc.c:1646  */
    break;

  case 91:
#line 679 "cfgram.y" /* yacc.c:1646  */
    {
		     if (cflex_include((yyvsp[-1].str), &(yylsp[-1])))
			     YYERROR;
		     free((yyvsp[-1].str));
	      }
#line 2238 "cfgram.c" /* yacc.c:1646  */
    break;

  case 92:
#line 690 "cfgram.y" /* yacc.c:1646  */
    {
		     current_rule->limits = (yyvsp[0].lrec);
	      }
#line 2246 "cfgram.c" /* yacc.c:1646  */
    break;

  case 93:
#line 696 "cfgram.y" /* yacc.c:1646  */
    {
		     char *p;
		     (yyval.lrec) = limits_record_create();
		     switch (limits_record_add((yyval.lrec), (yyvsp[0].str), &p)) {
		     case lrec_ok:
			     break;
		     case lrec_error:
			     cferror(&(yylsp[0]),
				     _("unrecognized resource limit: %s"),
				     p);
			     break;
		     case lrec_badval:
			     cferror(&(yylsp[0]),
				     _("bad value: %s"),
				     p);
			     break;
		     }
		     free((yyvsp[0].str));
	      }
#line 2270 "cfgram.c" /* yacc.c:1646  */
    break;

  case 94:
#line 716 "cfgram.y" /* yacc.c:1646  */
    {
		     char *p;
		     switch (limits_record_add((yyvsp[-1].lrec), (yyvsp[0].str), &p)) {
		     case lrec_ok:
			     break;
		     case lrec_error:
			     cferror(&(yylsp[-1]),
				     _("unrecognized resource limit: %s"),
				     p);
			     break;
		     case lrec_badval:
			     cferror(&(yylsp[-1]),
				     _("bad value: %s"),
				     p);
			     break;
		     }
		     free((yyvsp[0].str));
		     (yyval.lrec) = (yyvsp[-1].lrec);
	     }
#line 2294 "cfgram.c" /* yacc.c:1646  */
    break;

  case 95:
#line 741 "cfgram.y" /* yacc.c:1646  */
    {
		     struct transform_node *node;
		     size_t n;

		     n = strspn((yyvsp[-1].str) + 1, ":");
		     if ((yyvsp[-1].str)[n + 1]) {
			     struct cfloc loc;
			     loc.beg = (yylsp[-1]).beg;
			     loc.beg.column += n + 1;
			     loc.end = loc.beg;
			     cferror(&loc,
				     _("invalid character in short option designator"));
			     cferror(&loc,
				     _("short option letter can be followed only by zero to two colons"));
			     errors++;
		     } else {
			     if (n > 2) {
				     struct cfloc loc;
				     loc.beg = (yylsp[-1]).beg;
				     loc.beg.column += n;
				     loc.end = loc.beg;
				     cferror(&loc,
					     _("ignoring extra character in short option designator"));
				     cferror(&loc,
					     _("short option letter can be followed only by zero to two colons"));
			     }

			     node = new_transform_node(current_rule,
						       transform_remopt);
			     node->target.type = target_command;
			     node->v.remopt.s_opt = (yyvsp[-1].str);
			     node->v.remopt.l_opt = (yyvsp[0].str);
		     }
	     }
#line 2333 "cfgram.c" /* yacc.c:1646  */
    break;

  case 96:
#line 778 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.str) = NULL;
	     }
#line 2341 "cfgram.c" /* yacc.c:1646  */
    break;

  case 98:
#line 788 "cfgram.y" /* yacc.c:1646  */
    {
		     current_rule->clrenv = 1;
	      }
#line 2349 "cfgram.c" /* yacc.c:1646  */
    break;

  case 99:
#line 792 "cfgram.y" /* yacc.c:1646  */
    {
		     new_envar(current_rule,
			       (yyvsp[-2].str), strlen((yyvsp[-2].str)),
			       (yyvsp[0].str), strlen((yyvsp[0].str)),
			       envar_set);
		     free((yyvsp[-2].str));
		     free((yyvsp[0].str));
	      }
#line 2362 "cfgram.c" /* yacc.c:1646  */
    break;

  case 100:
#line 801 "cfgram.y" /* yacc.c:1646  */
    {
		      new_envar(current_rule,
				"", 0,
				(yyvsp[0].str), strlen((yyvsp[0].str)),
				envar_eval);
		      free((yyvsp[0].str));
	      }
#line 2374 "cfgram.c" /* yacc.c:1646  */
    break;

  case 101:
#line 809 "cfgram.y" /* yacc.c:1646  */
    {
		      add_asgn_list((yyvsp[0].asgn_list).head, envar_unset);
	      }
#line 2382 "cfgram.c" /* yacc.c:1646  */
    break;

  case 102:
#line 813 "cfgram.y" /* yacc.c:1646  */
    {
		      add_asgn_list((yyvsp[0].asgn_list).head, envar_keep);
	      }
#line 2390 "cfgram.c" /* yacc.c:1646  */
    break;

  case 103:
#line 819 "cfgram.y" /* yacc.c:1646  */
    {
		      (yyval.asgn_list).head = (yyval.asgn_list).tail = (yyvsp[0].asgn);
	      }
#line 2398 "cfgram.c" /* yacc.c:1646  */
    break;

  case 104:
#line 823 "cfgram.y" /* yacc.c:1646  */
    {
		      LIST_APPEND((yyvsp[0].asgn), (yyvsp[-1].asgn_list).head, (yyvsp[-1].asgn_list).tail);
		      (yyval.asgn_list) = (yyvsp[-1].asgn_list);
	      }
#line 2407 "cfgram.c" /* yacc.c:1646  */
    break;

  case 105:
#line 830 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.asgn) = xmalloc(sizeof(*(yyval.asgn)));
		     (yyval.asgn)->next = NULL;
		     (yyval.asgn)->name = (yyvsp[0].str);
		     (yyval.asgn)->value = NULL;
	      }
#line 2418 "cfgram.c" /* yacc.c:1646  */
    break;

  case 106:
#line 837 "cfgram.y" /* yacc.c:1646  */
    {
		     (yyval.asgn) = xmalloc(sizeof(*(yyval.asgn)));
		     (yyval.asgn)->next = NULL;
		     (yyval.asgn)->name = (yyvsp[-2].str);
		     (yyval.asgn)->value = (yyvsp[0].str);
	      }
#line 2429 "cfgram.c" /* yacc.c:1646  */
    break;

  case 107:
#line 849 "cfgram.y" /* yacc.c:1646  */
    {
		      (yyvsp[-1].attrib)(current_rule, (yyvsp[0].str), &(yylsp[0]));
		      free((yyvsp[0].str));
	      }
#line 2438 "cfgram.c" /* yacc.c:1646  */
    break;

  case 108:
#line 855 "cfgram.y" /* yacc.c:1646  */
    { cflex_pushargs(); }
#line 2444 "cfgram.c" /* yacc.c:1646  */
    break;

  case 109:
#line 856 "cfgram.y" /* yacc.c:1646  */
    {
		     int i;
		     struct argval *arg;

		     cflex_popargs();
		     (yyval.strlist).argc = (yyvsp[-1].arglist).argc;
		     (yyval.strlist).argv = xcalloc((yyvsp[-1].arglist).argc + 1, sizeof((yyval.strlist).argv[0]));
		     for (i = 0, arg = (yyvsp[-1].arglist).head; i < (yyvsp[-1].arglist).argc; i++, arg = arg->next) {
			     (yyval.strlist).argv[i] = arg->strval;
			     arg->strval = NULL;
		     }
		     arglist_free((yyvsp[-1].arglist).head);
	      }
#line 2462 "cfgram.c" /* yacc.c:1646  */
    break;


#line 2466 "cfgram.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 871 "cfgram.y" /* yacc.c:1906  */

void
yyerror(char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vcferror(&curloc, fmt, ap);
	va_end(ap);
	errors = 1;
}

void
cfgram_debug(int v)
{
#ifdef YYDEBUG
	yydebug = v;
#endif
}

struct rush_rule *
new_rush_rule(char const *tag)
{
	struct rush_rule *p = xzalloc(sizeof(*p));
	LIST_APPEND(p, rule_head, rule_tail);
	static unsigned rule_num = 0;

	rule_num++;
	if (tag && tag[0])
		p->tag = xstrdup(tag);
	else {
		char buf[INT_BUFSIZE_BOUND(unsigned)];
		char *s = uinttostr(rule_num, buf);
		p->tag = xmalloc(strlen(s) + 2);
		p->tag[0] = '#';
		strcpy(p->tag + 1, s);
	}

	p->mask = NO_UMASK;
	p->gid = NO_GID;
	p->fork = rush_undefined;
	p->acct = rush_undefined;
	return p;
}

struct transform_node *
new_transform_node(struct rush_rule *rule, enum transform_node_type type)
{
	struct transform_node *p = xzalloc(sizeof(*p));
	LIST_APPEND(p, rule->transform_head, rule->transform_tail);
	p->type = type;
	return p;
}

struct test_node *
new_test_node(enum test_type type)
{
	struct test_node *p = xzalloc(sizeof(*p));
	p->type = type;
	return p;
}

struct envar *
new_envar(struct rush_rule *rule,
	  char const *name, size_t nlen,
	  char const *value, size_t vlen,
	  enum envar_type type)
{
	struct envar *p = xmalloc(sizeof(*p)
				  + nlen + 1
				  + (value ? vlen + 1 : 0));
	p->next = NULL;
	p->name = (char*)(p + 1);
	memcpy(p->name, name, nlen);
	p->name[nlen] = 0;
	if (value) {
		p->value = p->name + nlen + 1;
		memcpy(p->value, value, vlen);
		p->value[vlen] = 0;
	} else {
		p->value = NULL;
	}

	p->type = type;
	LIST_APPEND(p, rule->envar_head, rule->envar_tail);
	return p;
}

static void
add_asgn_list(struct asgn *head, enum envar_type type)
{
	for (; head; head = head->next) {
		new_envar(current_rule,
			  head->name, strlen(head->name),
			  head->value, head->value ? strlen(head->value) : 0,
			  type);
		free(head->name);
		free(head->value);
	}
}

static struct transform_node *
new_set_node(enum transform_node_type type,
	     char *varname,
	     struct cfloc const *loc)
{
	struct transform_node *node;
	enum transform_target_type tgt;

	tgt = rush_variable_target(varname);
	if (tgt == target_readonly) {
		cferror(loc, _("attempt to modify a read-only variable"));
		errors++;
		return NULL;
	}
	node = new_transform_node(current_rule, type);
	node->target.type = tgt;
	switch (tgt) {
	case target_command:
	case target_program:
		free(varname);
		if (type == transform_delete) {
			cferror(loc,
				_("attempt to unset a read-only variable"));
			errors++;
			return NULL;
		}
		break;
	case target_var:
		node->target.v.name = varname;
		break;
	default:
		die(system_error, NULL,
		    _("INTERNAL ERROR at %s:%d: invalid target type %d"),
		    __FILE__, __LINE__,
		    tgt);
	}
	return node;
}
