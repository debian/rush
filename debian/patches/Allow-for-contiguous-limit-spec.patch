From 109ecb1df4bac34e7fdeb97e38e18e48d3ee2da3 Mon Sep 17 00:00:00 2001
From: Sergey Poznyakoff <gray@gnu.org>
Date: Wed, 14 Aug 2024 14:29:44 +0300
Subject: Allow for contiguous limit specifications.

* src/cfgram.y (resource_limits): Use collect_limits to parse
contiguous limit specifications.
* src/limits.c (parse_limits): Fix parsing of the legacy limits
specification.
---
 src/cfgram.y | 60 +++++++++++++++++++++----------------------
 src/limits.c | 84 +++++++++++++++++++++++-------------------------------------
 2 files changed, 61 insertions(+), 83 deletions(-)

Index: b/src/cfgram.y
===================================================================
--- a/src/cfgram.y
+++ b/src/cfgram.y
@@ -29,6 +29,8 @@
 static struct transform_node *new_set_node(enum transform_node_type type,
 					   char *varname,
 					   struct cfloc const *loc);
+static void collect_limits(struct cfloc const *loc, limits_record_t lrec,
+			   char *str);
 %}
 
 %error-verbose
@@ -398,7 +400,7 @@
 		     $$ = new_test_node(test_group);
 		     $$->v.groups = $2.argv;
 	     }
-           | TEST string
+	   | TEST string
 	     {
 		     $$ = new_test_node(test_fstest);
 		     $$->v.fstest.op = $1;
@@ -694,41 +696,13 @@
 
 resource_limits: IDENT
 	      {
-		     char *p;
 		     $$ = limits_record_create();
-		     switch (limits_record_add($$, $1, &p)) {
-		     case lrec_ok:
-			     break;
-		     case lrec_error:
-			     cferror(&@1,
-				     _("unrecognized resource limit: %s"),
-				     p);
-			     break;
-		     case lrec_badval:
-			     cferror(&@1,
-				     _("bad value: %s"),
-				     p);
-			     break;
-		     }
+		     collect_limits(&@1, $$, $1);
 		     free($1);
 	      }
 	    | resource_limits IDENT
 	      {
-		     char *p;
-		     switch (limits_record_add($1, $2, &p)) {
-		     case lrec_ok:
-			     break;
-		     case lrec_error:
-			     cferror(&@1,
-				     _("unrecognized resource limit: %s"),
-				     p);
-			     break;
-		     case lrec_badval:
-			     cferror(&@1,
-				     _("bad value: %s"),
-				     p);
-			     break;
-		     }
+		     collect_limits(&@1, $1, $2);
 		     free($2);
 		     $$ = $1;
 	     }
@@ -1006,3 +980,27 @@
 	}
 	return node;
 }
+
+static void
+collect_limits(struct cfloc const *loc, limits_record_t lrec, char *str)
+{
+	while (*str) {
+		char *p;
+
+		switch (limits_record_add(lrec, str, &p)) {
+		case lrec_ok:
+			break;
+
+		case lrec_error:
+			cferror(loc,
+				_("unrecognized resource limit: %s"),
+				p);
+			return;
+
+		case lrec_badval:
+			cferror(loc, _("bad value: %s"), p);
+			return;
+		}
+		str = p;
+	}
+}
Index: b/src/limits.c
===================================================================
--- a/src/limits.c
+++ b/src/limits.c
@@ -190,11 +190,11 @@
 }
 
 int
-getlimit(char **ptr, rlim_t *rlim, int mul)
+getlimit(char *ptr, char **endp, rlim_t *rlim, int mul)
 {
         unsigned long val;
 
-        val = strtoul(*ptr, ptr, 10);
+        val = strtoul(ptr, endp, 10);
         if (val == 0)
                 return 1;
         *rlim = val * mul;
@@ -245,55 +245,43 @@
 	case 'a':
 	case 'A':
 		/* RLIMIT_AS - max address space (KB) */
-		if (getlimit(&str, &lrec->limit_as, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_as, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_AS;
 		break;
 	case 't':
 	case 'T':
 		/* RLIMIT_CPU - max CPU time (MIN) */
-		if (getlimit(&str, &lrec->limit_cpu, 60)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_cpu, 60))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_CPU;
 		break;
 	case 'd':
 	case 'D':
 		/* RLIMIT_DATA - max data size (KB) */
-		if (getlimit(&str, &lrec->limit_data, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_data, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_DATA;
 		break;
 	case 'f':
 	case 'F':
 		/* RLIMIT_FSIZE - Maximum filesize (KB) */
-		if (getlimit(&str, &lrec->limit_fsize, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_fsize, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_FSIZE;
 		break;
 	case 'u':
 	case 'U':
 		/* RLIMIT_NPROC - max number of processes */
-		if (getlimit(&str, &lrec->limit_nproc, 1)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_nproc, 1))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_NPROC;
 		break;
 	case 'c':
 	case 'C':
 		/* RLIMIT_CORE - max core file size (KB) */
-		if (getlimit(&str, &lrec->limit_core, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_core, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_CORE;
 		break;
 	case 'm':
@@ -301,55 +289,45 @@
 		/* RLIMIT_MEMLOCK - max locked-in-memory
 		 * address space (KB)
 		 */
-		if (getlimit(&str, &lrec->limit_memlock, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_memlock, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_MEMLOCK;
 		break;
 	case 'n':
 	case 'N':
 		/* RLIMIT_NOFILE - max number of open files */
-		if (getlimit(&str, &lrec->limit_nofile, 1)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_nofile, 1))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_NOFILE;
 		break;
 	case 'r':
 	case 'R':
 		/* RLIMIT_RSS - max resident set size (KB) */
-		if (getlimit(&str, &lrec->limit_rss, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_rss, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_RSS;
 		break;
 	case 's':
 	case 'S':
 		/* RLIMIT_STACK - max stack size (KB) */
-		if (getlimit(&str, &lrec->limit_stack, 1024)) {
-			*endp = str;
+		if (getlimit(str, endp, &lrec->limit_stack, 1024))
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_STACK;
 		break;
 	case 'l':
 	case 'L': 
 		lrec->limit_logins = strtoul(str, &p, 10);
-		if (p == str) {
-			*endp = p;
+		*endp = p;
+		if (p == str)
 			return lrec_badval;
-		}
 		lrec->set |= SET_LIMIT_LOGINS;
 		break;
 	case 'p':
 	case 'P':
 		lrec->limit_prio = strtol(str, &p, 10);
-		if (p == str) {
-			*endp = p;
+		*endp = p;
+		if (p == str)
 			return lrec_badval;
-		}
 		if (lrec->limit_prio > 0)
 			lrec->set |= SET_LIMIT_PRIO;
 		break;
@@ -363,19 +341,21 @@
 int
 parse_limits(limits_record_t *plrec, char *str, char **endp)
 {
-        int c;
-        struct limits_rec *lrec = limits_record_create();
-	int rc;
-	while ((c = *str++)) {
-                if (ISWS(c))
-                        continue;
-		rc = limits_record_add(lrec, str, endp);
-		if (rc) {
-			free(lrec);
-			return rc;
+	struct limits_rec *lrec = limits_record_create();
+
+	while (*str) {
+		if (ISWS(*str))
+			str++;
+		else {
+			int rc;
+
+			rc = limits_record_add(lrec, str, &str);
+			if (rc) {
+				free(lrec);
+				return rc;
+			}
 		}
-        }
-        *plrec = lrec;
-        return 0;
+	}
+	*plrec = lrec;
+	return 0;
 }
-
